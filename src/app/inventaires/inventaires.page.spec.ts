import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventairesPage } from './inventaires.page';

describe('InventairesPage', () => {
  let component: InventairesPage;
  let fixture: ComponentFixture<InventairesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventairesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventairesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
