import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NouscontacterPage } from './nouscontacter.page';
import { Modal1Page } from '../modal1/modal1.page';

const routes: Routes = [
  {
    path: '',
    component: NouscontacterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NouscontacterPage, Modal1Page],
  entryComponents:[Modal1Page]
})
export class NouscontacterPageModule {}
