import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NouscontacterPage } from './nouscontacter.page';

describe('NouscontacterPage', () => {
  let component: NouscontacterPage;
  let fixture: ComponentFixture<NouscontacterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NouscontacterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NouscontacterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
