import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { Modal1Page } from '../modal1/modal1.page';

@Component({
  selector: 'app-nouscontacter',
  templateUrl: './nouscontacter.page.html',
  styleUrls: ['./nouscontacter.page.scss'],
})
export class NouscontacterPage implements OnInit {

  constructor(private modalCtrl: ModalController, private alertController: AlertController) { }

  ngOnInit() {
  }

async show(){
  const alert = await this.modalCtrl.create({
    component: Modal1Page
  });
  alert.present();
}


}
